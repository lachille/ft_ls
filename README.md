# ft_ls

Like the classic ls without some features. It's a 42 project where we had to code it from scratch.

See [subject](subject)

## Option :

	'-a' ->  all => has the priority on -A
	'-l' ->  line view
	'-t' ->  Sort by time modified (most recently modified first)
	'-r' ->  reverse sort
	'-R' ->  recursive
	'-A' ->  like -a but without '.' & '..'
	'-h' ->	 add suffix B,K,M,G,T,P   when use with -l  | Byte, Kilobyte,
			Megabyte, Gigabyte, Terabyte and Petabyte
	'-p' ->  add '/' at the end of filename if it's a directory
	'-G' -> enable colors

## Deployment

To deploy this project run

```bash
  make
  ./ft_ls
```


## Features

- Use bitwise filter for the options

## Screenshot
![alt text](img/Screenshot%202023-06-26%20183548.png)
